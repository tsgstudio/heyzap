package com.TSGStudio.Heyzap;

import android.util.Log;

import com.heyzap.sdk.ads.HeyzapAds;
import com.heyzap.sdk.ads.InterstitialOverlay;
import com.heyzap.sdk.ads.OnAdDisplayListener;

import com.unity3d.player.UnityPlayer;

/** Unity helper class for HeyzapAds */
public class HeyzapAdsHelper implements OnAdDisplayListener
{
	private static boolean hasStarted = false;

	/** Setup for HeyzapAds. */
	public static void Start(final int options)
	{
		if (hasStarted)
			return;

		UnityPlayer.currentActivity.runOnUiThread(new Runnable()
		{
			public void run()
			{
				final OnAdDisplayListener mListener = new HeyzapAdsHelper();
				HeyzapAds.start(UnityPlayer.currentActivity, options, mListener);
				//HeyzapAds.start(UnityPlayer.currentActivity, options);
			}
		});
		hasStarted = true;
	}

	/** Fetch the ads with specified name.
	 * @param tag The name of the ads. */
	public static void Fetch(String tag)
	{
		InterstitialOverlay.fetch(tag);
	}

	/** Display the ads with specified name.
	 * @param tag The name of the ads. */
	public static void Display(final String tag)
	{
		UnityPlayer.currentActivity.runOnUiThread(new Runnable()
		{
			public void run()
			{
				InterstitialOverlay.display(UnityPlayer.currentActivity, tag);
			}
		});
	}

	/** Whether the ads with specified name is ready to display.
	 * @param tag The name of the ads. */
	public static boolean IsReady(String tag)
	{
		return InterstitialOverlay.isAvailable(tag);
	}

	/** Hide current ads. */
	public static void Hide()
	{
		UnityPlayer.currentActivity.runOnUiThread(new Runnable()
		{
			public void run()
			{
				InterstitialOverlay.dismiss();
			}
		});	
	}

	/** This callback is raised after ads successfully fetched.
	 * @param tag The name of the ads. */
	public void onAvailable(String tag)
	{
		SendMessage("OnFetchSuccess", tag != null ? tag : "");
	}

	/** This callback is raised after ads failed to fetch.
	 * @param tag The name of the ads. */
	public void onFailedToFetch(String tag)
	{
		SendMessage("OnFetchFail", tag != null ? tag : "");
	}

	/** This callback is raised after ads successfully displayed.
	 * @param tag The name of the ads. */
	public void onShow(String tag)
	{
		SendMessage("OnDisplaySuccess", tag != null ? tag : "");
	}

	/** This callback is raised after ads failed to display.
	 * @param tag The name of the ads. */
	public void onFailedToShow(String tag)
	{
		SendMessage("OnDisplayFail", tag != null ? tag : "");
	}

	/** This callback is raised after ads hides.
	 * @param tag The name of the ads. */
	public void onHide(String tag)
	{
		SendMessage("OnHide", tag != null ? tag : "");
	}

	/** This callback is raised when ads receive click.
	 * @param tag The name of the ads. */
	public void onClick(String tag)
	{
		SendMessage("OnClick", tag != null ? tag : "");
	}

	/** Send message to the UnityPlayer.
	 * @param function The name of the function
	 * @param message The optional text parameter to send. */
	private static void SendMessage(String function, String message)
	{
		try
		{
			UnityPlayer.UnitySendMessage(HeyzapHelper.listenerName, function, message);
		}
		catch (Exception e)
		{
			Log.e("TSG", e.getMessage());
		}
	}
}
