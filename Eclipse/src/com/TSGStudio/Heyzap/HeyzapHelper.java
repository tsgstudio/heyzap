package com.TSGStudio.Heyzap;

import android.util.Log;

import com.heyzap.sdk.HeyzapLib;
import com.heyzap.sdk.HeyzapLib.LevelRequestListener;

import com.unity3d.player.UnityPlayer;

/** Unity helper class for HeyzapLib */
public class HeyzapHelper implements LevelRequestListener
{
	private static boolean hasStarted = false;

	/** The name of the GameObject which will receive Heyzap callback. */
	public static String listenerName = "[Heyzap Listener]";

	/** Start Heyzap setup. */
	public static void Start()
	{
		if (hasStarted)
			return;

		UnityPlayer.currentActivity.runOnUiThread(new Runnable()
		{
			public void run()
			{
				HeyzapLib.setLevelRequestListener(new HeyzapHelper());
				HeyzapLib.load(UnityPlayer.currentActivity, false);
			}
		});

		hasStarted = true;
	}

	/** Whether Heyzap is supported on this device
	 * @return True if supported; otherwise false. */
	public static boolean IsSupported()
	{
		return HeyzapLib.isSupported(UnityPlayer.currentActivity);
	}

	/** Set HeyzapLib settings flags.
	 * @param flags The flag value to set. */
	public static void SetFlags(int flags)
	{
		HeyzapLib.setFlags(flags);
	}

	/** Get HeyzapLib settings flags. */
	public static int GetFlags()
	{
		return HeyzapLib.getFlags();
	}

	/** Show the check-in form for this game, or show the "Get Heyzap" dialog
	 * @param message The optional message to send. */
	public static void CheckIn(String message)
	{
		HeyzapLib.checkin(UnityPlayer.currentActivity, message);
	}

	/** Submit player score for specified leader board.
	 * @param playerScore The amount of player scores.
	 * @param displayScore The text scores to display in leader board.
	 * @param leaderboard The name of the leader board table.
	 * @param skipDialog Whether to skip modal dialog. */
	public static void SubmitScore(int playerScore, String displayScore, String leaderboard, boolean skipDialog)
	{
		HeyzapLib.submitScore(UnityPlayer.currentActivity, Integer.toString(playerScore), displayScore, leaderboard, skipDialog);
	}

	/** Clear player saved scores preferences. */
	public static void ClearScores()
	{
		HeyzapLib.clearScorePrefs(UnityPlayer.currentActivity);
	}

	/** Display leader boards with specified name.
	 * @param leaderbord The optional leader boards name. */
	public static void ShowLeaderboards(String leaderbord)
	{
		HeyzapLib.showLeaderboards(UnityPlayer.currentActivity, leaderbord);
	}

	/** Display all player achievements. */
	public static void ShowAchievements()
	{
		HeyzapLib.showAchievements(UnityPlayer.currentActivity);
	}

	/** Post achievements. Show Achievement dialog on success.
	 * @param achievements A comma-separated list of achievement id. */
	public static void UnlockAchievements(String achievements)
	{
		HeyzapLib.unlockAchievement(UnityPlayer.currentActivity, achievements);
	}

	/** Send message to the UnityPlayer.
	 * @param function The name of the function
	 * @param message The optional text parameter to send. */
	private static void SendMessage(String function, String message)
	{
		try
		{
			UnityPlayer.UnitySendMessage(HeyzapHelper.listenerName, function, message);
		} catch (Exception e)
		{
			Log.e("TSG", e.getMessage());
		}
	}

	/** Create new instance of the HeyzapHelper. */
	private HeyzapHelper()
	{
	}

	/** This callback is raised when player wants to run specified level.
	 * @param level The name of the level to run. */
	public void onLevelRequested(String level)
	{
		if (level != null)
			SendMessage("OnLevelRequested", level);
	}
}
