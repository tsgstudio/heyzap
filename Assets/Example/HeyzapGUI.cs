namespace Heyzap
{
	using UnityEngine;

	/// <summary>
	/// The setup component for <see cref="Heyzap"/>.
	/// </summary>
	public class HeyzapGUI : MonoBehaviour
	{
		/// <summary>
		/// Whether to display debug GUI.
		/// </summary>
		public bool displayGUI = false;

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public void Awake ()
		{
			HeyzapHelper.AdsStateChange += OnAdsStateChange;
			HeyzapHelper.LevelRequest += OnLevelRequest;
		}

		#region GUI
		private string _playerScore = "1";
		private string _levelID = "XXX";

		/// <summary>
		/// OnGUI is called for rendering and handling GUI events.
		/// </summary>
		public void OnGUI ()
		{
			if (!displayGUI) return;

			GUILayout.BeginArea(new Rect(30, 30, Screen.width - 60, Screen.height - 30));

			GUILayout.BeginHorizontal();
			GUILayout.Label("Score", GUILayout.ExpandWidth(false));
			_playerScore = GUILayout.TextField(_playerScore, GUILayout.Width(200));
			GUILayout.Label("Level", GUILayout.ExpandWidth(false));
			_levelID = GUILayout.TextField(_levelID, GUILayout.Width(200));

			if (GUILayout.Button("Submit Score", GUILayout.Height(30)))
				HeyzapHelper.SubmitScore(int.Parse(_playerScore), _playerScore, _levelID);
			GUILayout.EndHorizontal();

			if (GUILayout.Button("CheckIn", GUILayout.Height(30)))
				HeyzapHelper.CheckIn("Test Message");

			if (GUILayout.Button("Fetch", GUILayout.Height(30)))
				HeyzapHelper.Fetch("Debug");

			if (GUILayout.Button("Display", GUILayout.Height(30)))
				HeyzapHelper.Display("Debug");

			GUILayout.EndArea();
		}
		#endregion

		private void OnLevelRequest (string level)
		{
			Debug.Log("OnLevelRequest: Name = " + level);
		}

		private void OnAdsStateChange (string adsName, HeyzapAdsState state)
		{
			Debug.Log("OnAdsStateChange: Name = " + adsName + " State = " + state);
		}
	}
}
